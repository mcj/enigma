public class Turing {
    public static void main(String[] args) {
        Enigma enigma1 = new Enigma('A', 'A', 'A');
        Enigma enigma2 = new Enigma('A', 'A', 'N');
        Enigma enigma3 = new Enigma('A', 'A', 'T');

        for (int i = 0; i < 17576; i++) {
            char c;
            c = enigma1.verschluesseln('t');
            c = enigma2.verschluesseln((char)(c+0x20));
            c = enigma3.verschluesseln((char)(c+0x20));
            if (c == 'T') {
                System.out.printf(" %s \n", enigma1.liesWalzen());
            }
        }
    }
}
