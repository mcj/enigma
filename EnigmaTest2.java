import java.io.Console;

public class EnigmaTest2 {
    public static void main(String args[]) {
        Enigma enigma = new Enigma('A', 'A', 'A');
        Console c = System.console();

        c.printf(" Bitte Schlüssel eingeben: ");
        String schluessel = c.readLine();
        if(schluessel.length() < 3 ||
                schluessel.charAt(0) < 'A' || schluessel.charAt(0) > 'Z' ||
                schluessel.charAt(1) < 'A' || schluessel.charAt(1) > 'Z' ||
                schluessel.charAt(2) < 'A' || schluessel.charAt(2) > 'Z' ) {
            schluessel = "AAA";
        }
        c.printf(" Schlüssel = %s\n", schluessel);

        c.printf(" Bitte Text eingeben: ");
        String klartext = c.readLine();
        klartext = klartext.toLowerCase();

        while(true) {
            enigma.setzeWalzen(schluessel.charAt(0),
                    schluessel.charAt(1),
                    schluessel.charAt(2));

            char z[] = new char[klartext.length()];
            for(int i = 0; i < klartext.length(); i++) {
                z[i] = klartext.charAt(i);
                z[i] = enigma.verschluesseln(z[i]);
            }

            String geheimtext = new String(z);
            String v;
            c.printf("\n%S\n", geheimtext);
            c.printf("\n Vertauschen: ");
            v = c.readLine();
            if(v.length() != 2)
                break;
            enigma.verbinden(v.charAt(0), v.charAt(1));
        }

        c.printf("\n");
    }
}
