/**
 * Die Enigma-Chiffriermaschine, wie im Buch
 * 'Geheime Botschaften - Die Kunst der Verschl&uuml;sselung von
 * der Antike bis in die Zeiten des Internet' von Simon
 * Singh.
 */
public class Enigma
{
    /**
     * Rotor Nr 1 der Enigma.
     * Jede Position im Array ist 'verdrahtet' mit
     * einer anderen Position. Das heisst, wenn '0' an Index
     * 3 im Array steht, ist 'D' (in der Abbildung im Buch
     * die rechte Spalte) mit 'A' (links davon) verbunden.
     * walze1 ist in der Abbildung im Buch rechts, walze2 in
     * der Mitte, walze3 links.
     */
    protected int[] walze1 =
    { 0,  9,  3, 10, 18,  8, 17, 20, 23,  1, 11,  7, 22,
      19, 12,  2, 16,  6, 25, 13, 15, 24,  5, 21, 14,  4};
    /**
     * Rotor Nr 2 der Enigma.
     * Siehe walze1.
     */
    protected int[] walze2 =
    { 4, 10, 12,  5, 11,  6,  3, 16, 21, 25, 13, 19, 14,
      22, 24,  7, 23, 20, 18, 15,  0,  8,  1, 17,  2,  9};
    /**
     * Rotor Nr 3 der Enigma.
     * Siehe walze1.
     */
    protected int[] walze3 =
    { 1,  3,  5,  7,  9, 11,  2, 15, 17, 19, 23, 21, 25,
      13, 24,  4,  8, 22,  6,  0, 10, 12, 20, 18, 16, 14};

    /**
     * Der Reflektor/die Umkehrwalze, nach dem gleichen Schema
     * wie die Walzen.
     */
    protected int[] reflektor =
    {24, 17, 20,  7, 16, 18, 11,  3, 15, 23, 13,  6, 14,
     10, 12,  8,  4,  1,  5, 25,  2, 22, 21,  9,  0, 19};

    /**
     * Das Steckerbrett, nach dem gleichen Schema wie die Walzen.
     * Wenn Ein Buchstabe mit dem Anderen 'verdrahtet'
     * ist, muss der Andere auch mit dem Einen verbunden
     * sein. Am Anfang sind alle Zeichen mit sich
     * selber verbunden.
     */
    protected int[] steckerbrett =
    { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12,
      13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};

    /**
     * Die Position von Walze1, von 0 bis 25. Werte gr&ouml;ßer 0
     * bedeuten, dass Die Walze, wenn man von der Abbildung im
     * Buch ausgeht, nach oben gedreht wird.
     */
    protected int pos1 = 0;
    /**
     * Die Position von Walze2. Siehe pos1.
     */
    protected int pos2 = 0;
    /**
     * Die Position von Walze3. Siehe pos1.
     */
    protected int pos3 = 0;

    /**
     * Initialisiert die Enigma mit Walzenlage w1, w1, w3.
     *
     * @param w1 Position f&uuml;r Walze 1. Muss zwischen 'A' und 'Z' sein.
     * @param w2 Position f&uuml;r Walze 2. Muss zwischen 'A' und 'Z' sein.
     * @param w3 Position f&uuml;r Walze 3. Muss zwischen 'A' und 'Z' sein.
     */
    public Enigma(char w1, char w2, char w3)
        throws IllegalArgumentException
    {
        setzeWalzen(w1, w2, w3);
    }

    /**
     * Initialisiert die Enigma mit Walzenlage 'A', 'A', 'A'.
     */
    public Enigma()
    {
        setzeWalzen('A', 'A', 'A');
    }

    /**
     * Die Walzen in Position w1, w2, w3 drehen.
     *
     * @param w1 Position f&uuml;r Walze 1. Muss zwischen 'A' und 'Z' sein.
     * @param w2 Position f&uuml;r Walze 2. Muss zwischen 'A' und 'Z' sein.
     * @param w3 Position f&uuml;r Walze 3. Muss zwischen 'A' und 'Z' sein.
     */
    public void setzeWalzen(char w1, char w2, char w3)
        throws IllegalArgumentException
    {
        if(w1 < 'A' || w1 > 'Z' ||
                w2 < 'A' || w2 > 'Z' ||
                w3 < 'A' || w3 > 'Z') {
            throw new IllegalArgumentException("w1, w2 und w3 müssen zwischen 'A' und 'Z' sein");
        }

        pos3 = (int) w1 - 0x41;
        pos2 = (int) w2 - 0x41;
        pos1 = (int) w3 - 0x41;
    }

    /**
     * Gibt die Positionen der Walzen als String mit drei
     * Zeichen zwischen 'A' und 'Z' zur&uuml;ck. Walze 1 ist
     * das letzte Zeichen.
     */
    public String liesWalzen()
    {
        String s;
        s = new String(new char[] {(char)(pos3+0x41),
                                   (char)(pos2+0x41),
                                   (char)(pos1+0x41)});
        return s;
    }

    /**
     * a und b am Steckerbrett verbinden. Wenn a und b schon
     * verbunden sind, wird die Verbindung getrennt.
     *<br>
     * Wenn Zeichen 'A' und 'B' verbunden sind, <i>sollte</i>
     * weder 'A' noch 'B' mit einem anderen Zeichen verbunden
     * werden, weil das an der echten Enigma nicht m&ouml;glich
     * war (hier ist es m&ouml;glich; es w&uuml;rde eine Art Dreieckverbindung
     * entstehen).<br>
     *
     * a und b m&uuml;ssen zwischen 'A' und 'Z' sein.
     */
    public void verbinden(char a, char b) throws IllegalArgumentException
    {
        if(a < 'A' || a > 'Z' ||
                b < 'A' || b > 'Z')
            throw new IllegalArgumentException("a und b müssen zwischen 'A' und 'Z' sein");

        int a2 = a - 0x41;
        int b2 = b - 0x41;
        int tmp;

        tmp = steckerbrett[a2];
        steckerbrett[a2] = steckerbrett[b2];
        steckerbrett[b2] = tmp;
    }

    /**
     * Zeichen c ver- oder entschl&uuml;sseln.
     *
     * c wird durch das Steckerbrett, die drei Walzen, den
     * Reflektor, wieder durch die Walzen und das Steckerbrett
     * ver&auml;ndert.
     * Wenn c nicht zwischen 'a' und 'z' (lowercase!) ist, wird '.'
     * zur&uuml;ckgegeben.<br>
     * Nach dem Ver-/Entschl&uuml;sseln werden die Walzen um eine Position
     * weitergedreht.
     *
     * @param c Das Zeichen, das ver-/entschl&uuml;sselt wird.
     * @return Das verschl&uuml;sselte c, entweder 'A' - 'Z', oder '.'.
     */
    public char verschluesseln(char c)
    {
        if(c < 'a' || c > 'z')
            return '.';

        int zeichen = c - 0x61;

        zeichen = steckerbrett[zeichen];

        zeichen = (zeichen + (walze1.length - pos1)) % walze1.length;
        zeichen = walze1[zeichen];
        zeichen = (zeichen + pos1) % walze1.length;

        zeichen = (zeichen + (walze2.length - pos2)) % walze2.length;
        zeichen = walze2[zeichen];
        zeichen = (zeichen + pos2) % walze2.length;

        zeichen = (zeichen + (walze3.length - pos3)) % walze3.length;
        zeichen = walze3[zeichen];
        zeichen = (zeichen + pos3) % walze3.length;

        zeichen = reflektor[zeichen];

        zeichen = (zeichen + (walze3.length - pos3)) % walze3.length;
        zeichen = indexVon(walze3, zeichen);
        zeichen = (zeichen + pos3) % walze3.length;

        zeichen = (zeichen + (walze2.length - pos2)) % walze2.length;
        zeichen = indexVon(walze2, zeichen);
        zeichen = (zeichen + pos2) % walze2.length;

        zeichen = (zeichen + (walze1.length - pos1)) % walze1.length;
        zeichen = indexVon(walze1, zeichen);
        zeichen = (zeichen + pos1) % walze1.length;

        zeichen = steckerbrett[zeichen];

        weiterdrehen();
        return (char) (zeichen + 0x41);
    }

    /**
     * Die Walzen um eine Position weiterdrehen.
     */
    protected void weiterdrehen()
    {
        pos1++;
        if( pos1 == walze1.length) {
            pos1 = 0;
            pos2++;
            if(pos2 == walze2.length) {
                pos2 = 0;
                pos3++;
                if(pos3 == walze3.length) {
                    pos3 = 0;
                }
            }
        }
    }

    /**
     * Hilfsmethode.
     * Erster Index von c in array; -1 falls nicht vorhanden.
     */
    private int indexVon(int[] array, int c)
    {
        for(int i = 0; i < array.length; i++)
            if(array[i] == c) return i;
        return -1;
    }
}
