import java.io.Console;

public class EnigmaTest {
    public static void main(String args[]) {
        Enigma enigma = new Enigma('A', 'A', 'A');
        Console c = System.console();

        c.printf(" Bitte Schlüssel eingeben: ");
        String schluessel = c.readLine();
        if(schluessel.length() < 3 ||
                schluessel.charAt(0) < 'A' || schluessel.charAt(0) > 'Z' ||
                schluessel.charAt(1) < 'A' || schluessel.charAt(1) > 'Z' ||
                schluessel.charAt(2) < 'A' || schluessel.charAt(2) > 'Z' ) {
            schluessel = "AAA";
        }
        c.printf(" Schlüssel = %s\n", schluessel);

        enigma.setzeWalzen(schluessel.charAt(0),
                schluessel.charAt(1),
                schluessel.charAt(2));

        while(true) {
            c.printf(" Vertauschen (AB): ");
            String s = c.readLine();
            if(s.length() == 2) {
                enigma.verbinden(s.charAt(0), s.charAt(1));
            }
            else {
                break;
            }
        }

        c.printf(" Bitte Text eingeben: ");
        String s = c.readLine();
        s = s.toLowerCase();

        for(int i = 0; i < s.length(); i++) {
            char z = s.charAt(i);
            c.printf("%c", enigma.verschluesseln(z));
        }

        c.printf("\n");
        c.readLine();
    }
}
