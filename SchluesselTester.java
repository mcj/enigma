import java.io.Console;

public class SchluesselTester {
    public static void main(String[] args) {
        Console cons = System.console();
        Enigma enigma = new Enigma();
        char[] z = {'r', 'o', 'm', 'd', 'p', 'r', 'u', 'x', 'h'};
        char[] e = new char[z.length];

        String s;
        while((s = cons.readLine()) != null) {
            enigma.setzeWalzen((char)(s.charAt(0)-0x41),
                    (char)(s.charAt(1)-0x41),
                    (char)(s.charAt(2)-0x41));
            for(int i = 0; i < z.length; i++) {
                char c = enigma.verschluesseln(z[i]);
                e[i] = c;
            }
            if(e[5] == e[2])
                cons.printf("\n\t%s \n", s);
        }
    }
}
