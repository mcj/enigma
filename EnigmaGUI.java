import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

public class EnigmaGUI extends JFrame
{
    Enigma enigma;
    Walze w1, w2, w3;
    Steckerbrett steckerbrett;
    JTextArea klartext; // (oder Geheimtext, je nachdem, ob man ver- oder entschluesseln will)
    JTextArea geheimtext; // (oder Klartext)

    public EnigmaGUI()
    {
        super ("Enigma");

        enigma = new Enigma();

        w1 = new Walze(this);
        w2 = new Walze(this);
        w3 = new Walze(this);

        // Alle Änderungen direkt übertragen.
        // Gefährlich - normalerweise sollte die Methode verbinden()
        // aufgerufen werden.
        steckerbrett = new Steckerbrett(this);
        enigma.steckerbrett = steckerbrett.verbindungen;

        klartext = new JTextArea(9, 20);

        // Bei Aenderungen im Klartext alles aktualisieren
        klartext.getDocument().addDocumentListener(new DocumentListener() {
                public void changedUpdate(DocumentEvent e)
                { aktualisieren(); }
                public void insertUpdate(DocumentEvent e)
                { aktualisieren(); }
                public void removeUpdate(DocumentEvent e)
                { aktualisieren(); }
            });

        geheimtext = new JTextArea(9, 20);
        geheimtext.setEditable(false);
        geheimtext.setBackground(new Color(230, 230, 230));

        JPanel walzen = new JPanel();
        walzen.add(w1);
        walzen.add(w2);
        walzen.add(w3);

        JPanel inhalt = new JPanel();
        inhalt.setLayout(new BoxLayout(inhalt, BoxLayout.Y_AXIS));
        inhalt.add(new JLabel("Walzen"));
        inhalt.add(walzen);
        inhalt.add(new JLabel("Steckerbrett"));
        inhalt.add(steckerbrett);
        inhalt.add(new JLabel("Klartext"));
        inhalt.add(klartext);
        inhalt.add(new JLabel("Geheimtext"));
        inhalt.add(geheimtext);

        this.add(inhalt);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**  Alle Daten aus TextAreas und den Walzen auslesen
     *  und den Text mit der Enigma verschluesseln.
     */
    public void aktualisieren()
    {
        enigma.setzeWalzen(w1.liesPosition(),
                w2.liesPosition(),
                w3.liesPosition());

        String s = klartext.getText().toLowerCase();
        char[] klar = new char[s.length()];
        s.getChars(0, s.length(), klar, 0);
        char[] geheim = new char[klar.length];

        for (int i = 0; i < klar.length; i++) {
            if (klar[i] == ' ' || klar[i] == '\n')
                geheim[i] = klar[i];
            else
                geheim[i] = enigma.verschluesseln(klar[i]);
        }

        geheimtext.setText(new String(geheim));
    }

    public static void main(String[] args)
    {
        new EnigmaGUI().setVisible(true);
    }
}

class Walze extends JPanel
    implements MouseListener
{
    protected char position = 'A'; // Das eingestellte Zeichen
    private boolean pfeilUntenGedrueckt = false;
    private boolean pfeilObenGedrueckt = false;
    private EnigmaGUI e; // zum Aktualisieren

    public Walze(EnigmaGUI e)
    {
        this.e = e;
        addMouseListener(this);
    }

    public char liesPosition()
    {
        return position;
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(3));
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int w = getWidth();
        int h = getHeight();

        if (pfeilObenGedrueckt)
            g.setColor(Color.GRAY);
        else
            g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h/3);

        if (pfeilUntenGedrueckt)
            g.setColor(Color.GRAY);
        else
            g.setColor(Color.WHITE);
        g.fillRect(0, h/3*2, w, h/3);

        // Pfeile
        g.setColor(Color.BLACK);
        g.drawLine((10), (h / 3 - 10), (w / 2), (h / 3 / 5));
        g.drawLine((w / 2), (h / 3 / 5), (w - 10), (h / 3 - 10));
        g.drawLine((10), ( h / 3 * 2 + 10), (w / 2), (h - 10));
        g.drawLine((w / 2), (h - 10), (w - 10), (h / 3 * 2 + 10));

        g.drawString(String.valueOf(position), (w / 2 - 7), (h / 2 + 2));
    }

    public Dimension getMinimumSize()
    {
        return new Dimension(20, 100);
    }

    public Dimension getPreferredSize()
    {
        return new Dimension(80, 140);
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        pfeilUntenGedrueckt = false;
        pfeilObenGedrueckt = false;
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        int wo = e.getY() / (getHeight() / 3);

        if (wo == 2) {
            pfeilUntenGedrueckt = true;
        } else if (wo == 0) {
            pfeilObenGedrueckt = true;
        }

        repaint();
    }

    // Die Walze Drehen
    @Override
    public void mouseClicked(MouseEvent e)
    {
        int wo = e.getY() / (getHeight() / 3);

        if (wo == 2) {
            position--;
            if (position < 'A')
                position = 'Z';
        } else if (wo == 0) {
            position++;
            if (position > 'Z')
                position = 'A';
        }

        repaint();
        this.e.aktualisieren();
    }

    // nicht benoetigt
    @Override
    public void mouseEntered(MouseEvent e){}
    @Override
    public void mouseExited(MouseEvent e){}
}

class Steckerbrett extends JPanel
    implements MouseListener, MouseMotionListener
{
    // zum Aktualisieren
    EnigmaGUI e;

    int[] verbindungen;

    // Beim Ziehen der Maus:
    int mausX, mausY; // Aktuelle Mausposition
    int auswahl = -1; // Der Buchstabe, der angeklickt wurde
    int auswahl2; // Der Buchstabe, zu dem eine neue Verbindung hergestellt werden soll

    public Steckerbrett(EnigmaGUI e)
    {
        this.e = e;

        verbindungen = new int[26];
        for (int i = 0; i < 26; i++)
            verbindungen[i] = i;

        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int w = getWidth();
        double w26 = w / 26.0;
        int h = getHeight();

        // Alphabete zeichnen
        g.setColor(Color.BLACK);
        double x = 2;
        for (char c = 'A'; c <= 'Z'; c++, x+=w26)
            g.drawString(String.valueOf(c), (int)x, 20);
        x = 2;
        for (char c = 'A'; c <= 'Z'; c++, x+=w26)
            g.drawString(String.valueOf(c), (int)x, h-6);

        // Verbindungen zeichnen
        g.setColor(Color.RED);
        for (int i = 0; i < verbindungen.length; i++) {
            if (verbindungen[i] != i) {
                g.drawOval((int)(i * w26) - 3, 7, 15, 15);
                g.drawOval((int)(verbindungen[i] * w26) - 3, h - 19, 15, 15);

                g.drawLine((int)(i * w26) + 3, 23,
                        (int)(verbindungen[i] * w26) + 3, h - 19);
            }
        }

        // Linie zwischen Maus und auswahl zeichnen
        if (auswahl > -1) {
            g.setColor(Color.BLUE);
            g.drawOval((int)(auswahl * w26) - 3, 7, 15, 15);
            g.drawLine((int)(auswahl * w26) + 3, 23, mausX, mausY);

            g.drawOval((int)(auswahl2 * w26) - 3, h - 19, 15, 15);
        }
    }

    public Dimension getPreferredSize()
    {
        return new Dimension(400, 110);
    }

    public void mouseDragged(MouseEvent e)
    {
        mausX = e.getX();
        mausY = e.getY();

        auswahl2 = mausX / (int)(getWidth() / 26);
        if (auswahl2 > 25) auswahl2 = 25;

        repaint();
    }

    public void mousePressed(MouseEvent e)
    {
        mausX = e.getX();
        mausY = e.getY();

        auswahl = e.getX() / (int)(getWidth() / 26);
        if (auswahl > 25) auswahl = 25;
        auswahl2 = auswahl;

        repaint();
    }

    // eine neue Verbindunge herstellen
    public void mouseReleased(MouseEvent e)
    {
        // evtl. Verbindungen trennen
        if (verbindungen[auswahl2] != auswahl2) {
            verbindungen[verbindungen[auswahl2]] = verbindungen[auswahl2];
            verbindungen[auswahl2] = auswahl2;
        }
        if (verbindungen[auswahl] != auswahl) {
            verbindungen[verbindungen[auswahl]] = verbindungen[auswahl];
            verbindungen[auswahl] = auswahl;
        }

        verbindungen[auswahl2] = auswahl;
        verbindungen[auswahl] = auswahl2;

        auswahl = -1;
        repaint();

        this.e.aktualisieren();
    }

    // Hilfsmethode, um ein Zeichen im Array zu finden
    private int indexVon(int[] array, int c)
    {
        for(int i = 0; i < array.length; i++)
            if(array[i] == c) return i;
        return -1;
    }

    // nicht benoetigt
    public void mouseExited(MouseEvent e) { }
    public void mouseEntered(MouseEvent e) { }
    public void mouseClicked(MouseEvent e) { }
    public void mouseMoved(MouseEvent e) { }
}
